package com.jpmc.theater;

import java.time.LocalDateTime;

public class Showing {
  private Movie movie;
  private int sequenceOfTheDay;
  private LocalDateTime showStartTime;
  private CalculateDiscounts calculateDiscounts = new CalculateDiscounts();

  public Showing(Movie movie, int sequenceOfTheDay, LocalDateTime showStartTime) {
    this.movie = movie;
    this.sequenceOfTheDay = sequenceOfTheDay;
    this.showStartTime = showStartTime;
  }

  public Movie getMovie() {
    return movie;
  }

  public LocalDateTime getStartTime() {
    return showStartTime;
  }

  public double getDiscount() {
    return calculateDiscounts.getLargestDiscountAmount(this);
  }

  public int getSequenceOfTheDay() {
    return sequenceOfTheDay;
  }

  public double getTotalPrice() {
    return movie.ticketPrice() - this.getDiscount();
  }

}
