package com.jpmc.theater;

/**
 * This class shows different discounts that can be applied to a movie showing.
 * There are three parts to each definition
 *
 * 1) If the discount is applicable to a showing
 * 2) Discount flat amount
 * 3) Discount percentage
 *
 */
public enum Discount implements IsDiscountApplicable {
  SPECIAL_MOVIE(0, 20) {
    public boolean isApplicable(Showing showing) {
      return showing.getMovie().specialCode() == 1;
    }
  },

  FIRST_SHOWING(3, 0) {
    @Override
    public boolean isApplicable(Showing showing) {
      return showing.getSequenceOfTheDay() == 1;
    }
  },

  SECOND_SHOWING(2, 0) {
    @Override
    public boolean isApplicable(Showing showing) {
      return showing.getSequenceOfTheDay() == 2;
    }
  },

  MATINEE(0, 25) {
    @Override
    public boolean isApplicable(Showing showing) {
      if (showing.getStartTime().getHour() > 11 && showing.getStartTime().getHour() < 16) {
        return true;
      } else
        return false;
    }
  },

  SEVENTH_SHOWING(1, 0) {
    @Override
    public boolean isApplicable(Showing showing) {
      return showing.getSequenceOfTheDay() == 7;
    }
  };

  private int flatDiscountAmount;
  private int discountPercent;

  public int getFlatDiscountAmount() {
    return this.flatDiscountAmount;
  }

  public int getDiscountPercent() {
    return this.discountPercent;
  }

  Discount(int flatDiscountAmount, int discountPercent) {
    this.flatDiscountAmount = flatDiscountAmount;
    this.discountPercent = discountPercent;
  }
}
