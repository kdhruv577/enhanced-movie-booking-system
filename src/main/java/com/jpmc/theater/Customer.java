package com.jpmc.theater;

public record Customer(String name, String id) {
}

/*
 * 
 * Redesigned the class to use Records - the classes whose objective is to
 * simply contain data and carry it between modules. Introduced in JAVA SE 14 as
 * a preview, however Java records help us to eliminate the need to define its
 * constructor, getter, and setter methods, along with eliminating the need to
 * use data structures like HashMap or print the contents of its objects as a
 * string, as we would need to override methods such as equals(), hashCode(),
 * and toString().
 * 
 **/
