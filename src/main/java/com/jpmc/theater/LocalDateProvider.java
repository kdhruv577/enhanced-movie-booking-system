package com.jpmc.theater;

import java.time.LocalDate;

/**
 * Using enum to ensure there is only one instance of LocalDateProvider
 */

public enum LocalDateProvider {
  INSTANCE;

  public LocalDate currentDate() {
    return LocalDate.now();
  }
}

