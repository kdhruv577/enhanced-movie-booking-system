package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class DiscountTests {

  @Test
  public void returnSpecialMovieDiscountIfApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
    Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.SPECIAL_MOVIE.isApplicable(showing);
    var expectedDiscountApplicable = true;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnSpecialMovieDiscountIfNotApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.SPECIAL_MOVIE.isApplicable(showing);
    var expectedDiscountApplicable = false;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnFirstShowingDiscountIfApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
    Showing showing = new Showing(spiderMan, 1, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.FIRST_SHOWING.isApplicable(showing);
    var expectedDiscountApplicable = true;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnFirstShowingDiscountIfNotApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.FIRST_SHOWING.isApplicable(showing);
    var expectedDiscountApplicable = false;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnSecondShowingDiscountIfApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 1);
    Showing showing = new Showing(spiderMan, 2, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.SECOND_SHOWING.isApplicable(showing);
    var expectedDiscountApplicable = true;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnSecondShowingDiscountIfNotApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 3, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.SECOND_SHOWING.isApplicable(showing);
    var expectedDiscountApplicable = false;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnSeventhShowingDiscountIfApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 7, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.SEVENTH_SHOWING.isApplicable(showing);
    var expectedDiscountApplicable = true;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnSeventhShowingDiscountIfNotApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 6, LocalDateTime.of(LocalDate.now(), LocalTime.of(9, 0)));

    var actualDiscountApplicable = Discount.SEVENTH_SHOWING.isApplicable(showing);
    var expectedDiscountApplicable = false;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnMatineeDiscountIfApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 7, LocalDateTime.of(LocalDate.now(), LocalTime.of(12, 0)));

    var actualDiscountApplicable = Discount.MATINEE.isApplicable(showing);
    var expectedDiscountApplicable = true;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

  @Test
  public void returnMatineeDiscountIfNotApplicable() {
    Movie spiderMan = new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 100, 0);
    Showing showing = new Showing(spiderMan, 6, LocalDateTime.of(LocalDate.now(), LocalTime.of(17, 0)));
    System.out.println(showing.getStartTime().getHour());

    var actualDiscountApplicable = Discount.MATINEE.isApplicable(showing);
    var expectedDiscountApplicable = false;

    assert (Boolean.compare(actualDiscountApplicable, expectedDiscountApplicable) == 0);
  }

}
