package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReservationTests {

  @Test
  void totalFee() {
    var customer = new Customer("John Doe", "unused-id");
    var showing = new Showing(
        new Movie("Spider-Man: No Way Home", Duration.ofMinutes(90), 12.5, 1),
        1,
        LocalDateTime.now());
    var reservation = new Reservation(customer, showing, 3);

    var expectedReservation = 28.125;
    var actualReservation = reservation.totalCost();
    assertTrue(Double.compare(expectedReservation, actualReservation) == 0);
  }
}

// Utilizing assert methods to determine pass/fail status of a junit test
