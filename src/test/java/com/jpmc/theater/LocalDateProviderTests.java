package com.jpmc.theater;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class LocalDateProviderTests {

  @Test
  void makeSureCurrentTime() {
    var expectedTime = LocalDate.now();
    var actualTime = LocalDateProvider.INSTANCE.currentDate();

    assert (expectedTime.compareTo(actualTime) == 0);
  }
}

// Utilizing assert methods to determine pass/fail status of a junit test
