package com.jpmc.theater;

import java.time.Duration;

public record Movie(String title, Duration runningTime, double ticketPrice, int specialCode) {
}

//Decoupled the discount logic in separate classes

/*
 * 
 * Redesigned the Movie class to use Records - the classes whose objective is to
 * simply contain data and carry it between modules. Introduced in JAVA SE 14 as
 * a preview, however Java records help us to eliminate the need to define its
 * constructor, getter, and setter methods, along with eliminating the need to
 * use data structures like HashMap or print the contents of its objects as a
 * string, as we would need to override methods such as equals(), hashCode(),
 * and toString().
 * 
 **/
