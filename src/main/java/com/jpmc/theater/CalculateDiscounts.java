package com.jpmc.theater;

import java.util.*;

/**
 * Introduced a decoupled class to calculate and filter the discount applicable for a
 * definite showing
 */
public class CalculateDiscounts {

  private List<Discount> discountsDefinitions = Arrays.asList(Discount.values());

  /**
   * Every element in the discountsDefinitions list represents filtered discount
   * that can be
   * used to calculate the largest discount for a given showing.
   */

  public double getLargestDiscountAmount(Showing showing) {
    return discountsDefinitions.stream()
        .filter(d -> d.isApplicable(showing))
        .map(d -> d.getFlatDiscountAmount() + showing.getMovie().ticketPrice() * d.getDiscountPercent() / 100)
        .sorted((a, b) -> Double.compare(b, a))
        .findFirst()
        .orElse(Double.valueOf(0));
  }

}

/**
 * The above method uses the steps to calculate the largest discount amount
 * possible for a discount definition
 *
 * 1) create a stream of all discounts
 * 2) filter for only discounts that are applicable to the showing
 * 3) calculate the total discount amount
 * 4) sort the discounts from highest to lowest
 * 5) return the highest discount, if there is no discount applicable default to
 * 0
 * 
 * @param showing
 * @return
 */
