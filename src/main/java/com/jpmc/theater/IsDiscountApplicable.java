package com.jpmc.theater;

public interface IsDiscountApplicable {
  boolean isApplicable(Showing showing);
}

/**
 * Created an interface to act as an outline for different discounts to be
 * considered
 **/
